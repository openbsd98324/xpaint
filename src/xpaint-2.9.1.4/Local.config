#ifndef XCOMM
#define XCOMM #
#endif

XCOMM Configuration portion of Imakefile for XPaint 2.7.2
XCOMM
XCOMM $Id: Local.config,v 1.18 2005/03/20 20:15:32 demailly Exp $

XCOMM Local customization

DESTDIR = 
SHAREDIR = /usr/share/xpaint

XCOMM Printing system defaults
PRINT_COMMAND = lpr

XCOMM Command that should produce a list of printers
XCOMM Disable this if your system does not support it, and use
XCOMM instead PRINTCAP (or disable it as well)
LPCCMD = /usr/sbin/lpc status
#define LPCCMD 

#if defined(__NetBSD__) || defined(__FreeBSD)
SHAREDIR = @PREFIX@/share/xpaint
LPCCMD = @PREFIX@/sbin/lpc status
#endif

XCOMM Enable/disable PRINTCAP (for BSD style printing configuration)
XCOMM PRINTCAP = /etc/printcap:/etc/printcap.cups
XCOMM #define PRINTCAP 

EDITOR = emacs -fn 9x15 -cr green -ms red -bg lightyellow -fg black
POSTSCRIPT_VIEWER = gv
EXTERN_VIEWER = display

XCOMM Size of tool icons; if "big", set option -DBIGTOOLICONS below
XCOMM otherwise comment TOOLFLAGS out
XCOMM Note: big icons are less color intensive for pseudocolor displays
XCOMM
XCOMM TOOLFLAGS = -DBIGTOOLICONS

XCOMM For FreeBSD, NetBSD and BSD386, no changes should be necessary, unless
XCOMM your libraries are in nonstandard places.

XCOMM For SunOS 4.1, include
XCOMM
XCOMM	   -DDECLARE_DRAND48
XCOMM
XCOMM Defines that may or may not be useful
XCOMM
XCOMM      -DMISSING_STRERROR if you system doesn't have strerror()
XCOMM      -DMISSING_STDARG_H if you system doesn't have <stdarg.h>
XCOMM      -DNOSTDHDRS        if you don't have <unistd.h> or <stdlib.h>
XCOMM 
XCOMM	   -DERRORBEEP        beep on errors
XCOMM 
XCOMM Defines for experimental features
XCOMM 
XCOMM      -DFEATURE_FRACTAL  for fractal fill operator
XCOMM 
XCOMM Defines for reinstating old behaviour
XCOMM 
XCOMM      -DFILTERNEEDSSELECTION	to get old behaviour of filter commands
XCOMM 

EXTRA_DEFINES = -DERRORBEEP -DFEATURE_FRACTAL $(TOOLFLAGS)

XCOMM 
XCOMM According to Craig Ian Dewick <craig@lios.apana.org.au>, this should not
XCOMM be necessary with Solaris 8 (SunOS 5.8).
XCOMM 
XCOMM #ifdef SunArchitecture
XCOMM ARCH_DEFINES = -DMISSING_STRERROR
XCOMM #endif

#ifdef NetBSDArchitecture
ARCH_DEFINES = -DHAVE_PARAM_H  
#ifdef arm32Architecture
CDEBUGFLAGS =
#endif
#endif

#ifdef OS2Architecture
ARCH_DEFINES = -DMISSING_STRCASECMP
#endif

XCOMM Need an ANSI C compiler
XCOMM
XCOMM  Which is either 'gcc' or 'acc' on a SparcStation
XCOMM   most MIPS based machines will do just fine using 'cc'
XCOMM
XCOMM CC = gcc
XCOMM CC = acc

XCOMM  If you have the TIFF library, make HaveTIFF defined
XCOMM    and make sure the path names to the library and 
XCOMM    includes are correct.
XCOMM

#define HaveTIFF

#ifdef HaveTIFF
TIFF_LIB     = -ltiff
TIFF_INCLUDE =
#endif

XCOMM  If you have the JPEG library, make HaveJPEG defined
XCOMM    and make sure the path names to the library and 
XCOMM    includes are correct.

#define HaveJPEG

#ifdef HaveJPEG
JPEG_LIB     = -ljpeg
JPEG_INCLUDE =
#endif

XCOMM  If you have the PNG library, make HavePNG defined
XCOMM    and make sure the path names to the library and 
XCOMM    includes are correct.

#define HavePNG

#ifdef HavePNG
PNG_LIB     = -lpng -lz
PNG_INCLUDE =
#endif

XCOMM  If you are running SunOS and get an error with the following undefined
XCOMM    symbols from the link line:
XCOMM       _get_wmShellWidgetClass
XCOMM       _get_applicationShellWidgetClass
XCOMM
XCOMM  try uncommenting these lines:
XCOMM
XCOMM #undef XawClientLibs
XCOMM #define XawClientLibs -lXaw -Bstatic -lXmu -Bdynamic -lXext -lX11
XCOMM

XCOMM  You must have the freetype2 library.
XFT_INCLUDE = -I/usr/include/freetype2
XFT_LIB = -lXft

XCOMM  You must have the XPM library.
XCOMM  Make sure the path name to the library is correct.

#if defined(__NetBSD__) || defined(__FreeBSD)
XPM_LIB     = -L@XPMDIR@/lib -Wl,-R@XPMDIR@/lib -lXpm -lX11 -lm -lXmu -lXext
#else
XPM_LIB     = -L/usr/lib -Wl,-R/usr/lib -lXpm -lX11 -lm -lXmu -lXext
#endif

XCOMM  Make sure this points to the location of the 'xpm.h' file.

#if defined(__NetBSD__) || defined(__FreeBSD)
XPM_INCLUDE = -I@XPMDIR@/include/X11
#else
XPM_INCLUDE = -I/usr/include/X11
#endif


DEPENDFLAGS = -I./bitmaps

XCOMM Uncomment these lines and tailor to taste, if you don't like
XCOMM the default paths and man page extension.
XCOMM
XCOMM BINDIR = /usr/local/bin
XCOMM MANDIR = /usr/local/man
XCOMM #undef ManSuffix
XCOMM #define ManSuffix 1

XCOMM Add '-g' here if you want to include debugging information.
XCOMM Also add any optimizer flags etc. that you want.
XCOMM
XCOMM	CDEBUGFLAGS = -g
XCOMM
XCOMM I have been told that compilation on AIX 3.2.5/IBM RS/6000 requires
XCOMM that you use the 'qlanglvl=ansi' flag.

XCOMM CDEBUGFLAGS =
#define PassCDebugFlags CDEBUGFLAGS='$(CDEBUGFLAGS)'

XCOMM This should take care of everything needed for BSD.

#if defined(__NetBSD__) || defined(__FreeBSD__)
TIFF_INCLUDE = -I@LOCALBASE@/include
TIFF_LIB     = -L@LOCALBASE@/lib -Wl,-R@LOCALBASE@/lib -ltiff
JPEG_INCLUDE = -I@LOCALBASE@/include
JPEG_LIB     = -L@LOCALBASE@/lib -Wl,-R@LOCALBASE@/lib -ljpeg
PNG_INCLUDE = -I@LOCALBASE@/include/png
PNG_LIB     = -L@LOCALBASE@/lib -Wl,-R@LOCALBASE@/lib -lpng -lz
XPM_INCLUDE = -I@XPMDIR@/include/X11
#endif
