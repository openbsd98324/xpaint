.\"                                      Hey, EMACS: -*- nroff -*-
.\" First parameter, NAME, should be all caps
.\" Second parameter, SECTION, should be 1-8, maybe w/ subsection
.\" other parameters are allowed: see man(7), man(1)
.TH IMGMERGE 1 "January 22, 2010"
.\" Please adjust this date whenever revising the manpage.
.\"
.\" Some roff macros, for reference:
.\" .nh        disable hyphenation
.\" .hy        enable hyphenation
.\" .ad l      left justify
.\" .ad b      justify to both left and right margins
.\" .nf        disable filling
.\" .fi        enable filling
.\" .br        insert line break
.\" .sp <n>    insert n+1 empty lines
.\" for manpage-specific macros, see man(7)
.SH NAME
IMGMERGE \- script to concatenate several images into a PDF file.
.br
.SH DESCRIPTION
This manual page documents briefly the
.B imgmerge
command.
.PP
\fBimgmerge\fP is batch script 'imgmerge' which uses xpaint to convert
and concatenate image files into a single PDF file. This also
makes use of Peter Szabo's 'pdfconcat'.
.SH SEE ALSO
.BR pdfconcat (1),
.BR xpaint (1).
.br
.SH AUTHOR
imgmerge was written by Jean-Pierre Demailly <Jean-Pierre.Demailly@ujf-grenoble.fr>
.PP
This manual page was written by Josue Abarca <jmaslibre@debian.org.gt>,
for the Debian project (and may be used by others). \&Permission is granted to copy, distribute and/or modify this document under the terms of the GNU General Public License, Version 2 or any later version published by the Free Software Foundation please see /usr/share/common-licenses/GPL-2 for the full text of the licence\&.
