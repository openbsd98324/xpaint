.\"                                      Hey, EMACS: -*- nroff -*-
.\" First parameter, NAME, should be all caps
.\" Second parameter, SECTION, should be 1-8, maybe w/ subsection
.\" other parameters are allowed: see man(7), man(1)
.TH PDFCONCAT 1 "January 22, 2010"
.\" Please adjust this date whenever revising the manpage.
.\"
.\" Some roff macros, for reference:
.\" .nh        disable hyphenation
.\" .hy        enable hyphenation
.\" .ad l      left justify
.\" .ad b      justify to both left and right margins
.\" .nf        disable filling
.\" .fi        enable filling
.\" .br        insert line break
.\" .sp <n>    insert n+1 empty lines
.\" for manpage-specific macros, see man(7)
.SH NAME
pdfconcat \- program to concatenate several PDF files.
.SH SYNOPSIS
.B pdfconcat
.RI \-o " outfile.pdf input1.pdf [inputN.pdf ...]"
.br
.SH DESCRIPTION
This manual page documents briefly the
.B pdfconcat
command.
.PP
.\" TeX users may be more comfortable with the \fB<whatever>\fP and
.\" \fI<whatever>\fP escape sequences to invode bold face and italics,
.\" respectively.
\fBpdfconcat\fP is a small and fast command\-line utility written in ANSI
C that can concatenate (merge) several PDF files into a long PDF
document. External libraries are not required, only ANSI C functions
are used.
Several features of the output file are taken from the first input file
only. For example, outlines (also known as hierarchical bookmarks) in
subsequent input files are ignored. pdfconcat compresses its input a
little bit by removing whitespace and unused file parts.
This program has been tested on various huge PDFs downloaded from the
Adobe web site, plus an 1200\-pages long mathematics manual typeset by
LaTeX, emitted by pdflatex, dvipdfm and `gs \-sDEVICE=pdfwrite', totalling
5981 pages in a single PDF file.

.SH OPTIONS
.TP
.B \-o "output.pdf"
Place output in file output.pdf
.SH SEE ALSO
.BR imgmerge (1),
.BR xpaint (1).
.br
.SH AUTHOR
pdfconcat was written by Peter Szabo <pts@fazekas.hu>.
.PP
This manual page was written by Josue Abarca <jmaslibre@debian.org.gt>,
for the Debian project (and may be used by others). \&Permission is granted to copy, distribute and/or modify this document under the terms of the GNU General Public License, Version 2 or any later version published by the Free Software Foundation please see /usr/share/common\-licenses/GPL\-2 for the full text of the licence\&.
