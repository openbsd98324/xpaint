

# norootforbuild

Name:		xpaint
Version:	2.9.1
Release:	2.pm.3.1

Summary:	A color image editing tool
License:	GPLv3
Group:		Productivity/Graphics/Bitmap Editors
URL:		http://sf-xpaint.sourceforge.net/

Source0:	%{name}-%{version}.tar.bz2

Patch0:		xpaint-upstream.patch

BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-buildroot

BuildRequires:	bison flex libjpeg-devel libpng-devel libtiff-devel update-desktop-files
BuildRequires:	xorg-x11-devel xorg-x11-libX11-devel xorg-x11-libXmu-devel
BuildRequires:	xorg-x11-libXpm-devel xorg-x11-libXt-devel xorg-x11-util-devel
BuildRequires:	xaw3d-devel


%description
Xpaint is a color image editing tool which features most standard paint program
options. It allows for the editing of multiple images simultaneously and
supports various formats, including PPM, XBM, TIFF, etc. 


%package devel
Group:		Productivity/Multimedia/Sound/Players
Summary:	Development package for %{name}
Requires:	%{name} = %{version}
Requires:	libjpeg-devel libpng-devel libtiff-devel update-desktop-files
Requires:	xorg-x11-libX11-devel xorg-x11-libXmu-devel xorg-x11-libXpm-devel
Requires:	xorg-x11-libXt-devel xorg-x11-util-devel

%description devel
Development package for %{name}.


%package -n libXaw3dxft6
Group:		System/Libraries
Summary:	Library needed by xpaint

%description -n libXaw3dxft6
Xaw3dxft library needed by xpain.


%debug_package


%prep
%setup -q
%patch0 -p1


%build
%suse_update_libdir configure
./configure
%__make %{?jobs:-j%{jobs}} xaw3dxft.so


%install
%makeinstall

%__mkdir_p %{buildroot}/%{_datadir}/pixmaps
%__install -D -m 644 icons/xpaint* %{buildroot}/%{_datadir}/pixmaps/

%__mkdir_p %{buildroot}/%{_datadir}/applications
%__install -D -m 644 xpaint.desktop %{buildroot}/%{_datadir}/applications/
%suse_update_desktop_file %{name}

%__mkdir_p %{buildroot}/%{_libdir}
%__install -D -m 644 xaw3dxft/libXaw3dxft.so.6 %{buildroot}/%{_libdir}/


%clean
rm -rf "%{buildroot}"


%files
%defattr(-,root,root,-)
%{_bindir}/imgmerge
%{_bindir}/pdfconcat
%{_bindir}/xpaint
%{_datadir}/applications/xpaint.desktop
%{_datadir}/pixmaps/xpaint*
%{_datadir}/X11/app-defaults/XPaint*
%dir %{_datadir}/xpaint
%{_datadir}/xpaint/XPaintIcon.xpm
%dir %{_datadir}/xpaint/bin
%{_datadir}/xpaint/bin/xpaint_ocr
%dir %{_datadir}/xpaint/bitmaps
%{_datadir}/xpaint/bitmaps/brushbox.cfg
%dir %{_datadir}/xpaint/bitmaps/brushes
%{_datadir}/xpaint/bitmaps/brushes/*.xpm
%dir %{_datadir}/xpaint/bitmaps/elec
%{_datadir}/xpaint/bitmaps/elec/*.xpm
%dir %{_datadir}/xpaint/help
%{_datadir}/xpaint/help/Help*
%dir %{_datadir}/xpaint/messages
%{_datadir}/xpaint/messages/Messages*


%files devel
%defattr(-,root,root,-)
%dir %{_datadir}/xpaint/include
%{_datadir}/xpaint/include/*.h
%dir %{_datadir}/xpaint/c_scripts/3d_curves
%{_datadir}/xpaint/c_scripts/3d_curves/*.c
%dir %{_datadir}/xpaint/c_scripts/3d_surfaces
%{_datadir}/xpaint/c_scripts/3d_surfaces/*.c
%dir %{_datadir}/xpaint/c_scripts
%dir %{_datadir}/xpaint/c_scripts/batch
%{_datadir}/xpaint/c_scripts/batch/*.c
%dir %{_datadir}/xpaint/c_scripts/filters
%{_datadir}/xpaint/c_scripts/filters/*.c
%dir %{_datadir}/xpaint/c_scripts/images
%{_datadir}/xpaint/c_scripts/images/*.c
%dir %{_datadir}/xpaint/c_scripts/layers
%{_datadir}/xpaint/c_scripts/layers/*.c
%dir %{_datadir}/xpaint/c_scripts/procedures
%{_datadir}/xpaint/c_scripts/procedures/*.c
%dir %{_datadir}/xpaint/c_scripts/templates
%{_datadir}/xpaint/c_scripts/templates/*.c

%files -n libXaw3dxft6
%defattr(-,root,root,-)
%{_libdir}/libXaw3dxft.so.6


%changelog
* Sun Jul 04 2010 - Detlef Reichelt <detlef@links2linux.de> - 2.9.1-2
- build with Xaw3dxft <2.9.1-2>

* Fri Jul 02 2010 - Detlef Reichelt <detlef@links2linux.de> - 2.9.1
- new upstream version <2.9.1>

* Tue Jan 05 2010 - Detlef Reichelt <detlef@links2linux.de> - 2.8.8
- initial build for PackMan <2.8.8>
