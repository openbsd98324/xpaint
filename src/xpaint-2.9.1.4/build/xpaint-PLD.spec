# $Revision: 1.53 $, $Date: 2010/02/15 07:56:56 $
Summary:	Paint program for X
Summary(de.UTF-8):	Malprogramm für X
Summary(es.UTF-8):	Programa de diseño para X
Summary(fr.UTF-8):	Programme de dessin sous X
Summary(pl.UTF-8):	Program do rysowania pod X Window
Summary(pt_BR.UTF-8):	Programa de desenho para X
Summary(tr.UTF-8):	X altında boyama programı
Name:		xpaint
Version:	2.8.16
Release:	1
License:	MIT
Group:		X11/Applications/Graphics
Source0:	http://dl.sourceforge.net/sf-xpaint/%{name}-%{version}.tar.bz2
# Source0-md5:	522c5a688fbcadc458fac6b227841622
Source1:	%{name}.desktop
Source2:	%{name}.png
URL:		http://sourceforge.net/projects/sf-xpaint/
BuildRequires:	libjpeg-devel
BuildRequires:	libpng-devel >= 2:1.4.0
BuildRequires:	libtiff-devel
BuildRequires:	xorg-cf-files
BuildRequires:	xorg-lib-libXaw-devel
BuildRequires:	xorg-lib-libXpm-devel >= 3.4c
BuildRequires:	xorg-util-imake
BuildRequires:	xorg-util-gccmakedep
Requires:	xorg-lib-libXt >= 1.0.0
BuildRoot:	%{tmpdir}/%{name}-%{version}-root-%(id -u -n)

%define		_appdefsdir	/usr/share/X11/app-defaults

%description
XPaint is a color image editing tool which features most standard
paint program options, as well as advanced features such as image
processing algorithms. It allows for the editing of multiple images
simultaneously and supp

%description -l de.UTF-8
XPaint ist ein Farbbildbearbeitungs-Tool mit den meisten üblichen,
aber auch erweiterten Funktionen wie Bildverarbeitungsalgorithmen. Sie
können mehrere Bilder gleichzeitig bearbeiten.

%description -l es.UTF-8
XPaint es una herramienta de edición de imágenes coloridas que
presenta la mayoría de las opciones padrón de programas de pintura,
así como características avanzadas como procesamiento de imágenes a
través de algoritmos. También permite la edición de múltiples imágenes
simultáneamente.

%description -l fr.UTF-8
xpaint est un outil d'édition d'images en couleur offrant la plupart
des options du programme paint, ainsi que des caractéristiques
avancées comme des algorithmes de traitement d'image. Il permet
l'édition simultanée de plusieurs images et plus.

%description -l pl.UTF-8
XPaint jest programem do edycji kolorowych grafik z funkcjami jakie ma
większość typowych programów tego typu, a także niektóre bardziej
zaawansowane funkcje, jak różne algorytmy obróbki grafiki.

%description -l pt_BR.UTF-8
XPaint é uma ferramenta de edição de imagens coloridas que apresenta a
maioria das opções-padrão de programas de pintura, assim como
características avançadas como processamento de imagens através de
algoritmos. Ele também permite a edição de múltiplas imagens
simultaneamente.

%description -l tr.UTF-8
XPaint, X ortamında en temel resimleme yeteneklerini barındıran basit
bir programdır.

%prep
%setup -q

%build
xmkmf
# to get stable results even if Xaw3d/neXtaw is installed
./configure xaw
%{__make} \
	CC="%{__cc}" \
	CXXDEBUGFLAGS="%{rpmcflags}" \
	CDEBUGFLAGS="%{rpmcflags}" \
	XPM_INCLUDE="-I/usr/include/X11"

%install
rm -rf $RPM_BUILD_ROOT
install -d $RPM_BUILD_ROOT{%{_desktopdir},%{_pixmapsdir}}

%{__make} install install.man \
	DESTDIR=$RPM_BUILD_ROOT \
	BINDIR=%{_bindir} \
	CONFDIR=%{_datadir}/X11 \
	MANDIR=%{_mandir}/man1

install %{SOURCE1} $RPM_BUILD_ROOT%{_desktopdir}
install %{SOURCE2} $RPM_BUILD_ROOT%{_pixmapsdir}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(644,root,root,755)
%doc README README.PNG TODO Doc/Operator.doc ChangeLog Doc/sample.Xdefaults
%attr(755,root,root) %{_bindir}/imgmerge
%attr(755,root,root) %{_bindir}/xpaint
%attr(755,root,root) %{_bindir}/pdfconcat
%{_appdefsdir}/XPaint
%lang(es) %{_appdefsdir}/XPaint_es
%lang(fr) %{_appdefsdir}/XPaint_fr
%dir %{_datadir}/xpaint
%{_datadir}/xpaint/XPaintIcon.xpm
%{_datadir}/xpaint/c_scripts
%{_datadir}/xpaint/include
%dir %{_datadir}/xpaint/bin
%{_datadir}/xpaint/bin/xpaint_ocr
%dir %{_datadir}/xpaint/bitmaps
%{_datadir}/xpaint/bitmaps/brushbox.cfg
%dir %{_datadir}/xpaint/bitmaps/brushes
%{_datadir}/xpaint/bitmaps/brushes/*.xpm
%dir %{_datadir}/xpaint/bitmaps/elec
%{_datadir}/xpaint/bitmaps/elec/*.xpm
%dir %{_datadir}/xpaint/help
%{_datadir}/xpaint/help/Help
%lang(es) %{_datadir}/xpaint/help/Help_es
%lang(fr) %{_datadir}/xpaint/help/Help_fr
%dir %{_datadir}/xpaint/messages
%{_datadir}/xpaint/messages/Messages
%lang(es) %{_datadir}/xpaint/messages/Messages_es
%lang(fr) %{_datadir}/xpaint/messages/Messages_fr
%{_mandir}/man1/xpaint.1*
%{_desktopdir}/xpaint.desktop
%{_pixmapsdir}/xpaint.png
%{_libdir}/X11/app-defaults

%define	date	%(echo `LC_ALL="C" date +"%a %b %d %Y"`)
%changelog
* %{date} PLD Team <feedback@pld-linux.org>
All persons listed below can be reached at <cvs_login>@pld-linux.org

$Log: xpaint.spec,v $
Revision 1.53  2010/02/15 07:56:56  amateja
- updated to 2.8.16

Revision 1.52  2010/02/01 10:13:15  duddits
- up to 2.8.14
- new URL

Revision 1.51  2010/01/29 07:46:59  amateja
- release 2

Revision 1.50  2010/01/20 08:59:36  arekm
- up to 2.8.13

Revision 1.49  2009/10/27 07:16:57  amateja
- updated to 2.8.6

Revision 1.48  2009/10/01 16:23:46  amateja
- updated to 2.8.2

Revision 1.47  2009/09/29 06:24:00  amateja
- updated to 2.8.0
- added BR xorg-util-gccmakedep
- added files in %files section

Revision 1.46  2009/07/12 13:25:21  arekm
- release 4

Revision 1.45  2009/07/10 19:53:35  arekm
- release 3

Revision 1.44  2008/11/07 20:52:05  arekm
- release 2

Revision 1.43  2007/02/12 22:09:25  glen
- tabs in preamble

Revision 1.42  2007/02/12 01:06:40  baggins
- converted to UTF-8

Revision 1.41  2006/10/17 20:43:37  qboosh
- updated to 2.7.8.1, removed obsolete errno patch
- switched to modular xorg

Revision 1.40  2006/04/19 18:02:37  megabajt
- updated URL and Source0

Revision 1.39  2006/03/09 14:35:11  glen
- quote %{__cc}

Revision 1.38  2004/11/10 19:57:51  ankry
- ven -> ve in desktop, rel. 8

Revision 1.37  2004/07/22 09:32:38  ankry
- desktop fixed, rel. 7

Revision 1.36  2004/07/04 20:37:43  adamg
- applnk -> vfolders; release 6

Revision 1.35  2003/12/25 12:28:26  qboosh
- use macro for app-defaults dir, dropped libpng 1.2.2 hack

Revision 1.34  2003/12/25 00:45:04  dzimi
- rel 5
- fix build on SYSV (errno.h)
- Xpaint merge to %{_prefix}/X11R6/lib/X11/app-defaults/ (fix package process)

Revision 1.33  2003/09/16 11:15:47  ankry
- md5

Revision 1.32  2003/09/16 08:34:50  adamg
- md5 sum added

Revision 1.31  2003/08/28 08:20:34  ankry
- cosmetics

Revision 1.30  2003/05/25 06:28:29  misi3k
- massive attack s/pld.org.pl/pld-linux.org/

Revision 1.29  2003/01/18 22:58:56  juandon
- removed two lines with define

Revision 1.28  2002/08/03 22:13:53  kloczek
- release 4: use new %doc.

Revision 1.27  2002/04/28 09:44:24  qboosh
- libpng 1.2.2 support

Revision 1.26  2002/02/25 21:05:12  filon
- fixed docs
- release 3

Revision 1.25  2002/02/23 05:29:55  kloczek
- adapterized.

Revision 1.24  2002/02/22 23:30:07  kloczek
- removed all Group fields translations (oure rpm now can handle translating
  Group field using gettext).

Revision 1.23  2002/01/18 02:15:43  kloczek
perl -pi -e "s/pld-list\@pld.org.pl/feedback\@pld.org.pl/"

Revision 1.22  2001/12/13 18:32:28  kloczek
- release 2.

Revision 1.21  2001/12/12 22:24:22  blues
- docs simplification

Revision 1.20  2001/12/06 06:08:23  kloczek
- updated to 2.6.2 (mostly bug fix version but also in PLD xpaint was long
  time not updated),
- updated URL and Source url,
- new Icon and png icon for deskto file,
- merge some translations from RH desktop file,
- removed zlib-devel (libpng-devel determines this).

Revision 1.19  2001/05/03 01:14:07  qboosh
- adapterized and made spec %%debug ready or added using %%rpm*flags macros

Revision 1.18  2000/11/12 16:58:52  kloczek
- removed xpm-devel from BuildRequires.

Revision 1.17  2000/07/28 13:59:21  zagrodzki
- release 3, built against libpng >= 1.0.8

Revision 1.16  2000/06/09 07:24:22  kloczek
- added using %%{__make} macro.

Revision 1.15  2000/04/01 11:16:02  zagrodzki
- changed all BuildRoot definitons
- removed all applnkdir defs
- changed some prereqs/requires
- removed duplicate empty lines

Revision 1.14  2000/03/28 16:55:25  baggins
- translated kloczkish into english

Revision 1.13  1999/09/07 15:30:23  kloczek
- release 2,
- fixed install path desktop file for xpaint.

Revision 1.12  1999/09/05 12:50:42  kloczek
- cosmetcs.

Revision 1.11  1999/09/04 09:44:01  pius
- cosmetics

Revision 1.10  1999/09/03 20:31:29  pius
- updated to 2.5.7,
- added xpaint.desktop instead of wmconfig file,
- fixed URL,
- cosmetics.

Revision 1.9  1999/07/20 12:48:12  wiget
- switch to rpm 3.0.2

Revision 1.8  1999/07/12 23:06:28  kloczek
- added using CVS keywords in %changelog (for automating them).

* Thu May 20 1999 Piotr Czerwiński <pius@pld.org.pl>
  [2.5.6-2]
- package is FHS 2.0 compliant,
- spec file based on RH version; rewritten for PLD use by me
  and Tomasz Kłoczko <kloczek@rudy.mif.pg.gda.pl>.
