Summary:	An X Window System image editing or paint program
Name:		xpaint
Version:	2.8.19
Release:	%mkrel 2
License:	MIT
Group:		Graphics
BuildRequires:	xpm-devel jpeg-devel png-devel libxp-devel
BuildRequires:	tiff-devel zlib-devel bison flex 
BuildRequires:	Xaw3d-devel xaw3dxft-devel imake gccmakedep
BuildRequires:	libxft-devel chrpath
Source0:	http://prdownloads.sourceforge.net/sf-xpaint/xpaint-%{version}.tar.bz2
Source1:	icons-%{name}.tar.bz2
Patch0:		xpaint-2.8.18-use_system_Xaw3dxft.patch
# patches from upstream
Patch1:		http://prdownloads.sourceforge.net/sf-xpaint/xpaint-2.8.19-minor_fixes.patch
Patch2:		http://prdownloads.sourceforge.net/sf-xpaint/xpaint-2.8.19-small-improvements.patch
URL:		https://sourceforge.net/projects/sf-xpaint
BuildRoot:	%{_tmppath}/xpaint-root
# Menus uses Liberation fonts
Requires:	fonts-ttf-liberation

%description
XPaint is an X Window System color image editing program which supports
many standard paint program operations. XPaint also supports advanced
features like image processing algorithms, scripting and batch jobs.  
XPaint allows you to edit multiple images simultaneously and supports
a large variety of image formats, including PNG, JPEG, TIFF, XPM, PPM, 
XBM, PS, etc.

Install this package if you need a simple paint program for X.

Recent versions of XPaint add new optional editing features based 
on programmable filters and user defined procedures written as scripts 
in plain C. The package includes a substantial list of examples and 
some support for batch processing.

%prep
%setup -q 
%patch0 -p1
%patch1 -p1
%patch2 -p1

%build
# adapted fixes from Fedora
sed -i -e "s/\(XCOMM CDEBUGFLAGS =\)/CDEBUGFLAGS = $RPM_OPT_FLAGS\nCXXDEBUGFLAGS = $RPM_OPT_FLAGS/g" Local.config
sed -i -e 's|-lXpm|-lXpm -lX11 -lm -lXmu -lXt -lXext|g' Local.config
sed -i -e 's|-lpng -lz|-lpng|g' Local.config
sed -i -e 's|/lib |/%{_lib} |g' Local.config
sed -i -e 's|@XPMDIR@|%{_prefix}|g' Local.config
sed -i -e 's|/usr/lib|%{_libdir}|g' configure
sed -i -e 's|install -c -s pdfconcat|install -c pdfconcat|g' Imakefile
sed -i -e 's|CFLAGS="-O3 -s -DNDEBUG=1"|CFLAGS=$RPM_OPT_FLAGS|g' pdfconcat.c
for f in ChangeLog README; do
    iconv -f iso-8859-1 -t utf-8 $f > $f.utf8
    touch -r $f $f.utf8
    mv $f.utf8 $f
done

#%%configure or %%configure2_5x brokes the build
./configure xaw3dxft.so

#%%make brokes the build
make

%install
rm -rf %{buildroot}

%makeinstall_std install.man

#mdk menu entry
mkdir -p %{buildroot}%{_datadir}/applications
cat > %{buildroot}%{_datadir}/applications/%{name}.desktop << EOF
[Desktop Entry]
Name=Xpaint
Comment=Paint program
Exec=%{name}
Icon=%{name}
Terminal=false
Type=Application
Categories=Graphics;
EOF

#mdk icon
install -d %{buildroot}%{_iconsdir}
tar jxf %{SOURCE1} -C %{buildroot}%{_iconsdir}

# rpath
chrpath -d %{buildroot}%{_bindir}/xpaint

# symlink on /etc
rm -rf %{buildroot}/usr/lib/X11/app-defaults

%if %mdkversion < 200900
%post
%update_menus
%endif

%if %mdkversion < 200900
%postun
%clean_menus
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc ChangeLog README* TODO Doc/*.doc Doc/sample.Xdefaults
%{_bindir}/*
%{_mandir}/man1/xpaint.1x*
%{_datadir}/xpaint
%config(noreplace) %{_sysconfdir}/X11/app-defaults/*
%{_datadir}/applications/%{name}.desktop
%{_iconsdir}/*.png
%{_iconsdir}/*/*.png


%changelog
* Thu Apr 15 2010 Jani Välimaa <wally@mandriva.org> 2.8.19-2mdv2010.1
+ Revision: 535092
- add patches from upstream

* Tue Apr 06 2010 Jani Välimaa <wally@mandriva.org> 2.8.19-1mdv2010.1
+ Revision: 532060
- new version 2.8.19
- fix .desktop file
- fix file list

* Sun Mar 28 2010 Jani Välimaa <wally@mandriva.org> 2.8.18-1mdv2010.1
+ Revision: 528608
- new version 2.8.18
- build against xaw3dxft
- require Liberation fonts as they are used in menus

* Tue Mar 09 2010 Jani Välimaa <wally@mandriva.org> 2.8.17-1mdv2010.1
+ Revision: 517238
- new version 2.8.17
- add minor fixes patch from upstream
- fix build
- adapt some fixes from Fedora
- fix xpaint binary rpath issue
- clean .spec

  + Sandro Cazzaniga <kharec@mandriva.org>
    - (I hope that) fix rebuild
    - Fix file list, hope that fix rebuilt
    - Fix file list
    - rebuild
    - %mkrel++
    - Drop patch from upstream to fix building
    - fix patching
    - Add two patch from upstream
    - Fix build with %%configure2_5x
    - Update to 2.8.15
    - fix rpath
    - update to 2.8.14
    - fix build
    - fix rpath and others rpmlint warning

* Thu Jan 28 2010 Sandro Cazzaniga <kharec@mandriva.org> 2.8.13.1-1mdv2010.1
+ Revision: 497511
- Update to 2.8.13.1

* Sat Jan 16 2010 Funda Wang <fwang@mandriva.org> 2.8.7.3-2mdv2010.1
+ Revision: 492273
- rebuild for new libjpeg v8

* Fri Nov 27 2009 Jérôme Brenier <incubusss@mandriva.org> 2.8.7.3-1mdv2010.1
+ Revision: 470594
- new version 2.8.7.3
- drop P0 and P1 (merged upstream)

* Thu Oct 01 2009 Frederic Crozat <fcrozat@mandriva.com> 2.8.2-2mdv2010.0
+ Revision: 452128
- Update description, based on author feedback

* Thu Oct 01 2009 Frederic Crozat <fcrozat@mandriva.com> 2.8.2-1mdv2010.0
+ Revision: 452018
- Release 2.8.2

* Mon Sep 28 2009 Frederic Crozat <fcrozat@mandriva.com> 2.8.0-1mdv2010.0
+ Revision: 450527
- Release 2.8.0
- Patch0: fix format string error
- Patch1: fix build with xaw3d

  + Thierry Vignaud <tv@mandriva.org>
    - rebuild
    - rebuild for new libjpeg

* Mon Aug 04 2008 Thierry Vignaud <tv@mandriva.org> 2.7.8.1-6mdv2009.0
+ Revision: 262658
- rebuild

* Thu Jul 31 2008 Thierry Vignaud <tv@mandriva.org> 2.7.8.1-5mdv2009.0
+ Revision: 257649
- rebuild

  + Pixel <pixel@mandriva.com>
    - rpm filetriggers deprecates update_menus/update_scrollkeeper/update_mime_database/update_icon_cache/update_desktop_database/post_install_gconf_schemas

* Mon Feb 04 2008 Thierry Vignaud <tv@mandriva.org> 2.7.8.1-3mdv2008.1
+ Revision: 162502
- rebuild
- fix 'error: for key "Icon" in group "Desktop Entry" is an icon name with an
  extension, but there should be no extension as described in the Icon Theme
  Specification if the value is not an absolute path'
- drop old menu
- fix removing file on x86_64
- BR gccmakedep
- BR imake
- kill re-definition of %%buildroot on Pixel's request
- buildrequires X11-devel instead of XFree86-devel
- import xpaint

  + Olivier Blin <oblin@mandriva.com>
    - restore BuildRoot


* Thu Aug 24 2006 Stew Benedict <sbenedict@mandriva.com> 2.7.8.1-2mdv2007.0
- P0: deal with new X/xpm paths, xdg menu

* Mon Feb 27 2006 Stew Benedict <sbenedict@mandriva.com> 2.7.8.1-1mdk
- 2.7.8.1
- fix invalid BuildRequires

* Sun Oct 02 2005 Nicolas L�cureuil <neoclust@mandriva.org> 2.7.7-3mdk
- BuildRequires fix

* Fri Jul 29 2005 Nicolas L�cureuil <neoclust@mandriva.org> 2.7.7-2mdk
- Fix BuildRequires

* Fri Jun 03 2005 Stew Benedict <sbenedict@mandriva.com> 2.7.7-1mdk
- 2.7.7

* Mon Apr 04 2005 Lenny Cartier <lenny@mandrakesoft.com> 2.7.6-1mdk
- 2.7.6

* Mon Mar 14 2005 Lenny Cartier <lenny@mandrakesoft.com> 2.7.4-1mdk
- 2.7.4

* Mon Jan 31 2005 Lenny Cartier <lenny@mandrakesoft.com> 2.7.3-1mdk
- 2.7.3

* Tue Oct 19 2004 Lenny Cartier <lenny@mandrakesoft.com> 2.7.2-1mdk
- 2.7.2

* Wed Sep 22 2004 Lenny Cartier <lenny@mandrakesoft.com> 2.7.1-1mdk
- 2.7.1

* Fri Aug 20 2004 Thierry Vignaud <tvignaud@mandrakesoft.com> 2.7.0-4mdk
- fix typo in menu entry

* Mon Dec 29 2003 Michael Scherer <misc@mandrake.org> 2.7.0-3mdk 
- fix BuildRequires ( remove lib )
- remove /usr/X11R6/lib/X11/app-defaults link to /etc

* Sun Sep 14 2003 Michael Scherer <scherer.michael@free.fr> 2.7.0-2mdk
- BuildRequires flex
 
* Mon Jun 16 2003 Stew Benedict <sbenedict@mandrakesoft.com> 2.7.0-1mdk
- 2.7.0

* Mon Apr 28 2003 Stew Benedict <sbenedict@mandrakesoft.com> 2.6.9-2mdk
- BuildRequires, distriblint
 
* Fri Apr  4 2003 Stew Benedict <sbenedict@mandrakesoft.com> 2.6.9-1mdk
- 2.6.9, new URL, Source tag 
- some new features finally, add patch1 to fix make install

* Mon Dec 30 2002 Stew Benedict <sbenedict@mandrakesoft.com> 2.6.2-2mdk
- rebuild for new glibc/rpm, add patch1 for errno

* Sat Nov 16 2002 Stew Benedict <sbenedict@mandrakesoft.com> 2.6.2-1mdk
- new version, add installed but unpackaged file, icons->png

* Fri Oct 19 2001 Sebastien Dupont <sdupont@mandrakesoft.com> 2.6.1-2mdk
- License
- srcs permissions
- remove patchs: xpaint-2.4.7-config.patch & xpaint-2.4.7-glibc.patch.

* Sun May 27 2001  Daouda Lo <daouda@mandrakesoft.com> 2.6.1-1mdk
- release 2.6.1
- stop Nono complains.
- cleanups

* Tue Oct 03 2000 Daouda Lo <daouda@mandrakesoft.com> 2.6.0-2mdk
- icons should be transparent
- let spec helper do its jobs
- menu entry in the body of the spec
- more macroz..

* Sun Aug 27 2000 Geoffrey Lee <snailtalk@mandrakesoft.com> 2.6.0-1mdk
- new and shiny version.

* Tue Aug 08 2000 Frederic Lepied <flepied@mandrakesoft.com> 2.4.9-16mdk
- automatically added BuildRequires

* Mon May 15 2000 David BAUDENS <baudens@mandrakesoft.com> 2.4-9-15mdk
- Fix build for i486
- Use %%{_tmppath} for BuildRoot

* Wed May 03 2000 dam's <damien@mandrakesoft.com> 2.4.9-14mdk
- Corrected icons.

* Tue Apr 18 2000 dam's <damien@mandrakesoft.com> 2.4.9-13mdk
- Convert gif icon to xpm.

* Mon Apr 17 2000 dam's <damien@mandrakesoft.com> 2.4.9-12mdk
- Added menu entry.

* Mon Mar 27 2000 dam's <damien@mandrakesoft.com> 2.4.9-11mdk
- Release.

* Tue Nov 02 1999 Pablo Saratxaga <pablo@mandrakesoft.com>
- corrected status of resource file

* Thu May 06 1999 Bernhard Rosenkraenzer <bero@mandrakesoft.com>
- Mandrake adaptions
- handle RPM_OPT_FLAGS

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 8)

* Thu Dec 17 1998 Michael Maher <mike@redhat.com>
- built package for 6.0

* Mon Aug  3 1998 Jeff Johnson <jbj@redhat.com>
- build root.

* Tue Jun 09 1998 Mike Wangsmo <wanger@redhat.com>
- changed the docs from being %%config files.

* Fri May 01 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Thu Apr 16 1998 Erik Troan <ewt@redhat.com>
- built against libpng 1.0

* Fri Oct 24 1997 Marc Ewing <marc@redhat.com>
- new release
- wmconfig

* Wed Oct 15 1997 Erik Troan <ewt@redhat.com>
- build against new libpng

* Thu Jul 31 1997 Erik Troan <ewt@redhat.com>
- built against glibc

* Tue Mar 25 1997 Erik Troan <ewt@redhat.com>
- "make install.man" places man page in wrong location
