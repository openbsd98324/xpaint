#define MESSAGES_VERSION "2.9.0"
#define DEFAULT_TITLE 0
#define CANT_FIND_PRINTCAP_FILE 1
#define USAGE_FOR 2
#define CANNOT_INSTALL_XPM_ICON 3
#define NO_MATCH_FOUND_FOR_VISUAL 4
#define BAD_VISUAL_TYPE_SPECIFICATION 5
#define YOUR_CANVAS_DEPTH_IS_THAT_MANY_BITS 6
#define SEPARATOR_STRIP_ONE 7
#define WARNING_ONE 8
#define SEPARATOR_STRIP_TWO 9
#define WARNING_TWO_A 10
#define WARNING_TWO_B 11
#define WARNING_THREE_A 12
#define WARNING_THREE_B 13
#define WARNING_THREE_C 14
#define GO_UP_ONE_DIRECTORY_LEVEL 15
#define NO_FILE_NAME_SUPPLIED 16
#define UNABLE_TO_GET_REGION 17
#define UNABLE_TO_CREATE_IMAGE 18
#define UNABLE_TO_CREATE_IMAGE_FOR_SAVING 19
#define UNABLE_TO_LOAD_FILTER 20
#define UNABLE_TO_LOAD_PROCEDURE 21
#define ERROR_SAVING_FILE 22
#define NO_REGION_SELECTED_PRESENTLY 23
#define NO_SUCH_FILE_OR_DIRECTORY 24
#define UNABLE_TO_OPEN_INPUT_FILE 25
#define IMAGE_FORMATS 26
#define END_IMAGE_FORMATS 44
#define POSITION 44
#define VALUE 45
#define RED_COLOR 46
#define GREEN_COLOR 47
#define BLUE_COLOR 48
#define RED_VARIANCE 49
#define GREEN_VARIANCE 50
#define BLUE_VARIANCE 51
#define SNAP_SPACING_X 52
#define SNAP_SPACING_Y 53
#define ENTER_DESIRED_SNAP_SPACING 54
#define LEVELS 55
#define NUMBER_OF_UNDO_LEVELS 56
#define IMAGE_NUMBER 57
#define RECALL_AN_IMAGE 58
#define REMOVE_AN_IMAGE 59
#define REMOVE_FILE 60
#define ZOOM 61
#define CHANGE_ZOOM_FACTOR_FOR_IMAGE 62
#define ANGLE_IN_DEGREES 63
#define ENTER_DESIRED_ROTATION 64
#define ENTER_TWOBYTWO_MATRIX_ENTRIES 65
#define ENTER_POINTS 66
#define MUST_BE_ODD 67
#define ENTER_MASK_SIZE_FOR_OIL_PAINT_EFFECT 68
#define ENTER_MASK_SIZE_FOR_SMOOTHING_EFFECT 69
#define ENTER_DESIRED_NOISE_VARIANCE 70
#define DISTANCE_PIXELS 71
#define ENTER_THE_DESIRED_SPREAD_DISTANCE 72
#define WIDTH_X_HEIGHT_OR_SINGLE_NUMBER 73
#define ENTER_DESIRED_MEGAPIXEL_SIZE 74
#define ENTER_MASK_SIZE_FOR_DESPECKLE_FILTER 75
#define BLACK_LEVEL 76
#define WHITE_LEVEL 77
#define ENTER_LEVELS_FOR_CONTRAST_ADJUSTMENT 78
#define ENTER_THRESHOLD_FOR_SOLARIZE_FILTER 79
#define ENTER_DESIRED_SHIFT_FOR_RGB_COMPONENTS 80
#define RED_SHIFT 81
#define GREEN_SHIFT 82
#define BLUE_SHIFT 83
#define ENTER_DESIRED_NUMBER_OF_COLORS 84
#define ENTER_DESIRED_ALPHA_CHANNEL_PARAMETERS 85
#define ALPHA_CHANNEL_SHOULD_BE_CREATED 86
#define ALPHA_MODE 87
#define ALPHA_BACKGROUND_VALUE 88
#define ALPHA_FOREGROUND_VALUE 89
#define ALPHA_THRESHOLD_VALUE 90
#define EXPANSION_PARAMETERS 91
#define EXPANSION_FACTOR 92
#define DO_INTERPOLATION 93
#define DOWNSCALING_PARAMETERS 94
#define DOWNSCALING_FACTOR 95
#define SIZE_WIDTH 96
#define SIZE_HEIGHT 97
#define SIZE_ZOOM 98
#define ENTER_DESIRED_IMAGE_SIZE 99
#define WIDTH_AND_DESIRED_LINE_STYLE 100
#define LINE_WIDTH 101
#define DASH_STYLE 102
#define CAP_STYLE 103
#define CAP_HINT 104
#define JOIN_STYLE 105
#define JOIN_HINT 106
#define GRID_DRAWING_MODE 107
#define GRID_LINES_OR_OTHER 108
#define GRID_USES_SNAP 109
#define GRID_COLOR 110
#define OPACITY 111
#define ENTER_THE_DESIRED_BRUSH_PARAMETERS 112
#define DENSITY 113
#define ENTER_THE_DESIRED_FRACTAL_DENSITY 114
#define DYNAMIC_WIDTH 115
#define DYNAMIC_MASS 116
#define DYNAMIC_DRAG 117
#define ENTER_THE_DESIRED_DYNAMIC_PENCIL_PARAMETERS 118
#define SPRAY_RADIUS 119
#define SPRAY_DENSITY 120
#define SPRAY_RATE 121
#define ENTER_THE_DESIRED_SPRAY_PARAMETERS 122
#define ARROWHEAD_TYPE 123
#define ARROWHEAD_SIZE 124
#define ARROWHEAD_ANGLE 125
#define ENTER_THE_DESIRED_ARROWHEAD_PARAMETERS 126
#define BOX_ROUND_CORNER_ABSOLUTE_SIZE 127
#define BOX_ROUND_CORNER_RELATIVE_SIZE 128
#define ENTER_THE_DESIRED_ROUND_CORNER_SIZE 129
#define NUMBER_OF_SIDES_OF_POLYGON 130
#define STARLIKE_POLYGON_INNER_RATIO 131
#define ENTER_THE_DESIRED_SPECIAL_POLYGON_PARAMETERS 132
#define GRADIENT_ANGLE 133
#define GRADIENT_PAD 134
#define GRADIENT_HORIZONTAL_OFFSET 135
#define GRADIENT_VERTICAL_OFFSET 136
#define GRADIENT_STEPS 137
#define ENTER_THE_DESIRED_GRADIENT_FILL_PARAMETERS 138
#define PATTERN_WIDTH 139
#define PATTERN_HEIGHT 140
#define ENTER_THE_DESIRED_PATTERN_SIZE 141
#define YOU_MUST_HAVE_AT_LEAST_ONE_ENTRY 142
#define NO_INFORMATION_TO_SAVE 143
#define UNABLE_TO_OPEN_FILE 144
#define UNABLE_TO_OPEN_FILE_FOR_WRITING 145
#define OVERWRITE_FILE 146
#define UNSAVED_CHANGES_WISH_TO_QUIT 147
#define UNSAVED_CHANGES_WISH_TO_CLOSE 148
#define UNSAVED_CHANGES_WISH_TO_REVERT 149
#define RESIZING_WARNING_CANNOT_BE_UNDONE 150
#define AUTOCROP_WARNING_CANNOT_BE_UNDONE 151
#define ARE_YOU_SURE_YOU_WANT_TO_CROP_THE_IMAGE_TO_THE_SIZE_OF_THE_REGION 152
#define BAD_SNAP_SPACING 153
#define BAD_NUMBER_OF_UNDO_LEVELS 154
#define INVALID_ZOOM 155
#define INVALID_MASK_SIZE 156
#define INVALID_NOISE_VARIANCE 157
#define INVALID_SPREAD_DISTANCE 158
#define INVALID_PIXEL_SIZE 159
#define INVALID_WHITE_LEVEL 160
#define INVALID_BLACK_LEVEL 161
#define INVALID_SOLARIZATION_THRESHOLD 162
#define INVALID_RGB_PERCENTAGES 163
#define INVALID_NUMBER_OF_COLORS 164
#define INVALID_SCALING_FACTOR 165
#define INVALID_ALPHA_CHANNEL_PARAMETERS 166
#define UNABLE_TO_CREATE_PAINT_WINDOW_WITH_IMAGE 167
#define FILE_WRITTEN 168
#define FILE_SENT_TO_PRINTER 169
#define INVALID_WIDTH_MUST_BE_GREATER_THAN_ZERO 170
#define DASH_IS_GIVEN_BY_ALTERNATING_THAT_MANY_CHARACTERS 171
#define OPACITY_SHOULD_BE_BETWEEN_ZERO_AND_HUNDRED_PERCENT 172
#define WIDTH_SHOULD_BE_BETWEEN_ONE_AND_ONE_HUNDRED 173
#define MASS_SHOULD_BE_BETWEEN_TEN_AND_TWO_THOUSAND 174
#define DRAG_SHOULD_BE_BETWEEN_ZERO_AND_FIFTY 175
#define RADIUS_SHOULD_BE_BETWEEN_ONE_AND_ONE_HUNDRED 176
#define DENSITY_SHOULD_BE_BETWEEN_ONE_AND_FIVE_HUNDRED 177
#define RATE_SHOULD_BE_BETWEEN_ONE_AND_FIVE_HUNDRED 178
#define TYPE_SHOULD_BE_BETWEEN_ONE_AND_MAXTYPE 179
#define SIZE_SHOULD_BE_BETWEEN_ONE_AND_ONE_THOUSAND 180
#define ANGLE_SHOULD_BE_BETWEEN_ZERO_AND_SIXTY 181
#define NUMBER_OF_SIDES_SHOULD_BE_BETWEEN_THREE_AND_ONE_HUNDRED 182
#define BOX_ROUND_CORNER_ABSOLUTE_SIZE_SHOULD_BE_BETWEEN_ZERO_AND_ONE_THOUSAND 183
#define BOX_ROUND_CORNER_RELATIVE_SIZE_SHOULD_BE_BETWEEN_ZERO_AND_ONE 184
#define BOX_ROUND_CORNER_ABSOLUTE_AND_RELATIVE_SIZE_CANNOT_BE_BOTH_POSITIVE 185
#define INNER_RATIO_SHOULD_BE_BETWEEN_ZERO_AND_ONE 186
#define HORIZONTAL_OFFSET_SHOULD_BE_BETWEEN_PLUS_MINUS_HUNDRED_PERCENT 187
#define VERTICAL_OFFSET_SHOULD_BE_BETWEEN_PLUS_MINUS_HUNDRED_PERCENT 188
#define PAD_SHOULD_BE_BETWEEN_PLUS_MINUS_FOURTY_NINE_PERCENT 189
#define ANGLE_SHOULD_BE_BETWEEN_PLUS_MINUS_THREE_HUNDRED_SIXTY_DEGREES 190
#define NUMBER_OF_STEPS_SHOULD_BE_BETWEEN_ONE_AND_THREE_HUNDRED 191
#define LOST_ERROR_TERM 192
#define FONT_SELECT_DESIRED_PROPERTIES 193
#define UNABLE_TO_LOAD_REQUESTED_FONT 194
#define YOU_MUST_FIRST_COMPILE_A_C_SCRIPT 195
#define C_SCRIPT_SUCCESSFULLY_COMPILED_AND_LINKED 196
#define C_SCRIPT_COULD_NOT_BE_COMPILED_OR_LINKED 197
#define INVALID_WIDTH 198
#define INVALID_HEIGHT 199
#define INVALID_WIDTH_OR_HEIGHT_MUST_BE_GREATER_THAN_ZERO 200
#define INVALID_WIDTH_OR_HEIGHT_MUST_BE_LESS_THAN_HUNDRED_TWENTY_NINE 201
#define INVALID_WIDTH_OR_HEIGHT_MUST_BE_BETWEEN_ZERO_AND_TWO_HUNDRED_FIFTY_SIX 202
#define DIRECTORIES 203
#define FILES 204
#define LIST_LOADED_FILES 205
#define FILE_DOES_NOT_EXIST 206
#define FILE_NOT_IN_THE_LIST 207
#define OPEN_FILE_NAMED 208
#define SAVE_IN_FILE 209
#define LOAD_FROM_FILE 210
#define IMAGE_NUMBER_OUT_OF_RANGE 211
#define OUT_OF_MEMORY 212
#define UNABLE_TO_ALLOCATE_SUFFICIENT_COLOR_ENTRIES 213
#define TRY_EXITING_OTHER_COLOR_INTENSIVE_APPLICATIONS 214
#define SHOULDNT_HAPPEN 215
#define ERROR_IN_XINTERNATOM 216
#define CANNOT_GRAB_POINTER 217
#define INSUFFICIENT_RESOURCES_FOR_OPERATION 218
#define PROBABLY_NOT_ENOUGH_MEMORY_TO_ALLOCATE_PIXMAP 219
#define ERROR_CODE 220
#define UNKNOWN_IMAGE_FORMAT 221
#define OUT_OF_MEMORY_ALLOCATING_A_ROW 222
#define OUT_OF_MEMORY_ALLOCATING_AN_ARRAY 223
#define READ_ERROR 224
#define WRITE_ERROR 225
#define A_FILE_READ_ERROR_OCCURRED_AT_SOME_POINT 226
#define A_FILE_WRITE_ERROR_OCCURRED_AT_SOME_POINT 227
#define JUNK_IN_FILE_WHERE_BITS_SHOULD_BE 228
#define READ_ERROR_READING_MAGIC_NUMBER 229
#define JUNK_IN_FILE_WHERE_AN_INTEGER_SHOULD_BE 230
#define CANT_HAPPEN 231
#define MAXVAL_IS_TOO_LARGE 232
#define BAD_MAGIC_NUMBER_NOT_A_PPM_PGM_OR_PBM_FILE 233
#define OUT_OF_MEMORY_COMPUTING_HASH_TABLE 234
#define OUT_OF_MEMORY_ALLOCATING_HASH_TABLE 235
#define OUT_OF_MEMORY_GENERATING_HISTOGRAM 236
#define SAME_COLOR_FOUND_TWICE 237
#define COLORHIST_OUT_OF_MEMORY 238
#define FATAL_LIBPNG_ERROR_LONGJMP_CALLED_WHILE_READING 239
#define FATAL_LIBPNG_ERROR_LONGJMP_CALLED_WHILE_WRITING 240
#define ERROR_PLTE_CHUNK_NOT_FOUND_IN_PALETTE_IMAGE 241
#define STRIPPING_48_BIT_RGB_IMAGE_TO_24_BITS 242
#define STRIPPING_16_BIT_GRAYSCALE_IMAGE_TO_8_BITS 243
#define STRIPPING_64_BIT_RGBA_IMAGE_TO_32_BITS 244
#define STRIPPING_32_BIT_GRAY_ALPHA_IMAGE_TO_16_BITS 245
#define UNKNOWN_PNG_IMAGE_TYPE 246
#define READPNG_CANT_ALLOCATE_MEMORY_FOR_ROW_POINTERS_1 247
#define READPNG_CANT_ALLOCATE_MEMORY_FOR_ROW_POINTERS_2 248
#define UNABLE_TO_ALLOCATE_TEMPORARY_STORAGE_FOR_ALPHA_IMAGE 249
#define READPNG_UNABLE_TO_MALLOC_PNG_DATA 250
#define WRITEPNG_CANT_USE_ALPHA_MASK_WITH_COLORMAPPED_IMAGE 251
#define WRITEPNG_SORRY_CANT_WRITE_ALPHA_IMAGES_YET 252
#define WRITEPNG_CANT_ALLOCATE_MEMORY_FOR_ROW_POINTERS 253
#define XPAINT_FATAL_LIBPNG_ERROR 254
#define XPAINT_EXTREMELY_FATAL_LIBPNG_ERROR 255
#define END_MESSAGES 256

extern String *msgText;
extern int msgNum;
