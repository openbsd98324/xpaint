# xpaint

## Installation

````
 ./configure ; make 
Compiling by default with modified xaw3dxft widgets.
Other available options : auto / Xaw, Xaw3d, Xaw95, neXtaw

XAWLIB_DEFINES = -DXAW3D -DXAW3DG -DXAW3DXFT
SYS_LIBRARIES = xaw3dxft/libXaw3dxft.a -lXt -L.

./configure: line 66: xmkmf: command not found
make: *** No rule to make target 'Xaw3dP.h'.  Stop.
./configure: line 67: xmkmf: command not found
make: *** No targets specified and no makefile found.  Stop.
````

## Medias

![](medias/1692354087-screenshot.png)

![](medias/graph.jpg)



## Manjaro

````
970dab81831cf48562d2b427e3aa9587  /usr/local/bin/xpaint

readelf -d /usr/local/bin/xpaint 

Dynamic section at offset 0xc2cd0 contains 38 entries:
  Tag        Type                         Name/Value
 0x0000000000000001 (NEEDED)             Shared library: [libXpm.so.4]
 0x0000000000000001 (NEEDED)             Shared library: [libX11.so.6]
 0x0000000000000001 (NEEDED)             Shared library: [libm.so.6]
 0x0000000000000001 (NEEDED)             Shared library: [libXmu.so.6]
 0x0000000000000001 (NEEDED)             Shared library: [libXext.so.6]
 0x0000000000000001 (NEEDED)             Shared library: [libXft.so.2]
 0x0000000000000001 (NEEDED)             Shared library: [libtiff.so.6]
 0x0000000000000001 (NEEDED)             Shared library: [libjpeg.so.62]     (MANDAORY)  
 0x0000000000000001 (NEEDED)             Shared library: [libpng16.so.16]
 0x0000000000000001 (NEEDED)             Shared library: [libXaw3dxft.so.6]  (MANDAORY)  
 0x0000000000000001 (NEEDED)             Shared library: [libXt.so.6]
 0x0000000000000001 (NEEDED)             Shared library: [libfontconfig.so.1]
 0x0000000000000001 (NEEDED)             Shared library: [libc.so.6]
````


